package fr.iutlille.ctp_2122;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Candidature {

    private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE);
    private String nom;
    private String web;
    private Etat etat;
    private Date limite;

    public enum Etat { TODO, LATE, DONE, NOPE }

    public static List<Candidature> LoadCandidaturesFromResources(Context context, int resId)
            throws JSONException, IOException {
        StringBuilder text = new StringBuilder();
        BufferedReader br = new BufferedReader(new
                InputStreamReader(context.getResources().openRawResource(resId)));
        String line;
        while ((line = br.readLine()) != null) {
            text.append(line);
            text.append('\n');
        }
        br.close();
        JSONArray array = new JSONArray(text.toString());
        return LoadCandidatures(array);
    }

    private static List<Candidature> LoadCandidatures(JSONArray json) throws JSONException {
        Candidature[] candidatures;
        candidatures = new Candidature[json.length()];
        for (int i = 0; i < json.length(); i++) {
            candidatures[i] = new Candidature(json.getJSONObject(i));
        }
        return Arrays.asList(candidatures);
    }

    public Candidature(JSONObject json) throws JSONException {
        this.nom = json.getString("nom");
        this.web = json.getString("web");
        this.etat = Etat.valueOf(json.getString("etat"));
        try {
            this.limite = df.parse(json.getString("limite"));
        } catch (ParseException e) {
            this.limite = new Date();
        }
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public Date getLimite() {
        return limite;
    }

    public void setLimite(long limite) {
        this.limite.setTime(limite);
    }
}
