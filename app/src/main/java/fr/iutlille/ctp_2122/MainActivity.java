package fr.iutlille.ctp_2122;  // ligne à modifier


import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import fr.iutlille.ctp_2122.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private final int active = 0;
    private ActivityMainBinding ui;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ui = ActivityMainBinding.inflate(this.getLayoutInflater());
        setContentView(ui.getRoot());
        ParcoursBUT application = (ParcoursBUT) getApplication();
        /* TODO Q1: statistiques */
        /* TODO Q3a: Menu contextuel */
        /* TODO Q3: RecyclerView */
    }

    /* TODO Q1: statistiques */

    /* TODO Q2: A propos */

    /* TODO Q3a: Menu contextuel */

    /* TODO Q3b: Tri de liste */

}